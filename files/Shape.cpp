#include "Shape.h"
#include <string.h>
#include <iostream>

using namespace std;
Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

string Shape::getType() const
{
	return this->_type;
}

string	Shape::getName() const
{
	return this->_name;
}

