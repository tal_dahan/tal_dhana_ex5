#include "Triangle.h"



void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name):Polygon(type, name)
{
	this->_a = a;
	_points.push_back(a);
	this->_b = b;
	_points.push_back(b);
	this->_c = c;
	_points.push_back(c);
}

Triangle::~Triangle()
{
	/*there is no memort to relice*/
}

double Triangle::getArea() const
{
	/*caculate area of triangle by 3 points*/
	double dArea = ((this->_b.getX() - this->_a.getX())*(this->_c.getY() - this->_a.getY()) - (this->_c.getX() - this->_a.getX())*(this->_b.getY() -this->_a.getY())) / 2.0;
	if (dArea < 0)
	{
		dArea = dArea * -1;
	}

	return dArea;
}

double Triangle::getPerimeter() const
{
	double preimeter = this->_a.distance(this->_b) + this->_a.distance(this->_c) + this->_b.distance(this->_c);
	return preimeter;
}

void Triangle::move(const Point& other)
{
	this->_a += other;
	this->_b += other;
	this->_c += other;
}