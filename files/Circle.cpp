#include "Circle.h"

Circle::Circle(const Point& center, double radius, const string& type, const string& name) :Shape(name, type)
{
	this->_center = center;
	this->_radius = radius;
}
Circle::~Circle()
{
	/*there is no memort to relice*/
}


const Point& Circle::getCenter() const
{
	return this->_center;
}
double Circle::getRadius() const
{
	return this->_radius;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}


double Circle::getArea() const
{
	return (PI * pow(this->_radius, 2));
}

double Circle::getPerimeter() const
{
	return (PI * 2 * this->_radius);
}

void Circle::move(const Point& other)
{
	this->_center = other;
}


