#include "Menu.h"

using namespace std;


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::openScrean()
{
	cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit. "<< endl;
}

void Menu::AddNewShape()
{
	cout<<"Enter 0 to add a circle.\
		\nEnter 1 to add an arrow.\
		\nEnter 2 to add a triangle.\
		\nEnter 3 to add a rectangle." <<endl;
}

void Menu::addCircle()
{
	double x = 0, y = 0, radius = 0;
	string name;
	cout << "Please enter X:" << endl;
	cin >> x;
	cout << "Please enter Y:" << endl;
	cin >> y;
	cout << "Please enter radius:" << endl;
	cin >> radius;
	getchar();//clean the buffer
	cout << "Enter the name of the shape:" << endl;
	std::getline(std::cin, name);
	this->_shape.push_back( new Circle(Point(x, y), radius, "Circle", name));//add the shape to the vector
	/*drow the shape*/
	this->_shape[_shape.size() - 1]->draw(*_disp,*_board);
}

void Menu::addArrow()
{
	int x1, x2, y1, y2;
	string name;
	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;
	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;
	getchar();//clean the buffer
	cout << "Enter the name of the shape:" << endl;
	std::getline(std::cin, name);
	this->_shape.push_back(new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", name));//add the shape to the vector 
	/*drow the shape*/
	this->_shape[_shape.size() - 1]->draw(*_disp, *_board);
	
}

void Menu::addTriangle()
{
	int x1, x2, y1, y2,x3,y3;
	string name;
	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;
	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;
	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;
	cout << "Enter the X of point number: 2" << endl;
	cin >> x3;
	cout << "Enter the Y of point number: 2" << endl;
	cin >> y3;
	getchar();//clean the buffer
	cout << "Enter the name of the shape:" << endl;
	std::getline(std::cin, name);
	if ((x1 == x2 && x2 == x3) || (y1 == y2 && y1 == y3))
	{
		cout << "The points entered create a line." << endl;
		system("pause");
	}
	else
	{
		this->_shape.push_back(new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "Triangle", name));//add the shape tp the vector
	/*drow the shape*/
		this->_shape[_shape.size() - 1]->draw(*_disp, *_board);
	}
	
}


void Menu::addRectangle()
{
	int x, y, length, width;
	string name;
	cout << "Enter the X of the to left corner:" << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner:" << endl;
	cin >> y;
	cout << "Please enter the length of the shape:" << endl;
	cin >> length;
	cout << "Please enter the width of the shape:" << endl;
	cin >> width;

	getchar();//clean the buffer
	cout << "Enter the name of the shape:" << endl;
	std::getline(std::cin, name);
	if (width == 0 || length == 0)
	{
		cout << "Length or Width can't be 0" << endl;
		system("pause");
	}
	else
	{
		this->_shape.push_back(new myShapes::Rectangle(Point(x, y), length, width, "Rectangle", name));//add the shape to the vector
	/*drow the shape*/
		this->_shape[_shape.size() - 1]->draw(*_disp, *_board);
	}
	
}

void Menu::info()
{
	int choice = -1;
	int secChoice = -1;
	int x, y;
	if (this->_shape.size() >  0)
	{
		/*wait for valid choice*/
		while (choice < 0 || choice >= this->_shape.size())
		{
			system("CLS");
			/*print the ditails of the shapes*/
			for (int i = 0; this->_shape.size() > i; i++)
			{
				cout << "Enter " + to_string(i) + " for " + this->_shape[i]->getName() + "(" + this->_shape[i]->getType() + ")" << endl;
			}
			cin >> choice;
			getchar();//clean the buffer
		}
		
		cout << "Enter 0 to move the shape" << endl;
		cout << "Enter 1 to get its details." << endl;
		cout << "Enter 2 to remove the shape." << endl;
		cin >> secChoice;
		getchar();//clean the buffer

		/*check validation*/
		while (secChoice<0 || secChoice > 3)
		{
			system("CLS");//clean the screan
			cout << "Enter 0 to move the shape" << endl;
			cout << "Enter 1 to get its details." << endl;
			cout << "Enter 2 to remove the shape." << endl;
			cin >> secChoice;
			getchar();//clean the buffer
		}
		

		
		switch (secChoice)
		{
		case MOVE:
			system("CLS");//clean the screan
			cout << "Please enter the X moving scale:" << endl;
			cin >> x;
			getchar();
			cout << "Please enter the Y moving scale:" << endl;
			cin >> y;
			getchar();
			this->_shape[choice]->clearDraw(*_disp, *_board);
			this->_shape[choice]->move(Point(x, y));
			for (int i = 0; i < this->_shape.size(); i++)
			{
				_shape[i]->draw(*_disp, *_board);
			}

			break;
		case DITAILS:
			cout << this->_shape[choice]->getType() + "  " + this->_shape[choice]->getName() + "       " + to_string(this->_shape[choice]->getArea()) + " " + to_string(this->_shape[choice]->getPerimeter());
			system("pause");
			break;
		case REMOVE:
			this->_shape[choice]->clearDraw(*_disp, *_board);
			this->_shape.erase(this->_shape.begin() + choice);
			for (int i = 0; i < this->_shape.size(); i++)
			{
				_shape[i]->draw(*_disp, *_board);
			}
			break;
		}
	}
	
}

void Menu::deleteAll()
{
	int i = 0;

	for (i = 0; i < _shape.size(); i++)
	{
		_shape[i]->clearDraw(*_disp, *_board);
		_shape.clear();
	}
}


