#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <string>


#define NEW_SHAPE 0
#define INFO 1
#define DEL 2
#define EXIT 3

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3

#define MOVE 0
#define DITAILS 1
#define REMOVE 2


class Menu
{
public:

	Menu();
	~Menu();
	void openScrean();//print the opening screen
	void AddNewShape();//print the shpes choosing screen

	/*functions that respons for output and the input to creat new shapes */
	void addCircle();
	void addArrow();
	void addTriangle();
	void addRectangle();

	void deleteAll();
	void info();
	
	// more functions..

private: 

	std::vector<Shape *> _shape;//creta vector of type shape 
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

