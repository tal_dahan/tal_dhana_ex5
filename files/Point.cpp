#include "Point.h"

using namespace std;
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}


/*deep copy*/
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::Point()
{

}

Point::~Point()
{
	/*nothing to do*/
}


double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(this->_x - other._x, 2) + pow(this->_y - other._y, 2));
}

Point Point::operator+(const Point& other) const
{
	Point newP = Point(other.getX() + _x, other.getY() + _y);
	return newP;
}

Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	return *this;
}

