#include "Menu.h"

using namespace std;
using namespace myShapes;

int main()
{	
	Menu m;
	int choice = -1;
	int shapeC = -1;

	string name;

	


	while (choice != 3)
	{
		system("CLS");
		while (choice<0 || choice>3)
		{
			m.openScrean();
			std::cin >> choice;
			getchar();
			system("CLS");

		}
		
		shapeC = -1;
		switch (choice)
		{
		case NEW_SHAPE:
			while (shapeC < 0 || shapeC > 3)
			{
				system("CLS");
				m.AddNewShape();
				std::cin >> shapeC;
				getchar();
			}
		
			switch (shapeC)
			{
			case CIRCLE:
				m.addCircle();
				break;
			case ARROW:
				m.addArrow();
				break;
			case TRIANGLE:
				m.addTriangle();
				break;
			case RECTANGLE:
				m.addRectangle();
				break;

			}
			break;
		case INFO:
			m.info();
			break;
		case DEL:
			m.deleteAll();
			break;
		}
		if (choice != 3)
		{
			choice = -1;
		}
		
	}

	

	return 0;


	
}